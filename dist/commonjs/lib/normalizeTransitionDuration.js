'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _default = function _default(duration, type) {
  return typeof duration === 'number' || typeof duration === 'string' ? duration : duration[type];
};

/**
 * Normalizes the duration of a transition.
 * @param {number|object} duration The value to normalize.
 * @param {'hide'|'show'} type The type of transition.
 * @returns {number}
 */
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(_default, 'default', 'src/lib/normalizeTransitionDuration.js');
}();

;