'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _without2 = require('lodash/without');

var _without3 = _interopRequireDefault(_without2);

var _set2 = require('lodash/set');

var _set3 = _interopRequireDefault(_set2);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _uniq2 = require('lodash/uniq');

var _uniq3 = _interopRequireDefault(_uniq2);

var _some2 = require('lodash/some');

var _some3 = _interopRequireDefault(_some2);

var _has2 = require('lodash/has');

var _has3 = _interopRequireDefault(_has2);

var _isArray2 = require('lodash/isArray');

var _isArray3 = _interopRequireDefault(_isArray2);

var _last2 = require('lodash/last');

var _last3 = _interopRequireDefault(_last2);

var _forEach2 = require('lodash/forEach');

var _forEach3 = _interopRequireDefault(_forEach2);

var _isBrowser = require('./isBrowser');

var _isBrowser2 = _interopRequireDefault(_isBrowser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EventStack = function () {
  function EventStack() {
    var _this = this;

    (0, _classCallCheck3.default)(this, EventStack);
    this._handlers = {};
    this._pools = {};

    this._emit = function () {
      return _this.___emit__REACT_HOT_LOADER__.apply(_this, arguments);
    };

    this._normalize = function () {
      return _this.___normalize__REACT_HOT_LOADER__.apply(_this, arguments);
    };

    this._listen = function () {
      return _this.___listen__REACT_HOT_LOADER__.apply(_this, arguments);
    };

    this._unlisten = function () {
      return _this.___unlisten__REACT_HOT_LOADER__.apply(_this, arguments);
    };

    this.sub = function () {
      return _this.__sub__REACT_HOT_LOADER__.apply(_this, arguments);
    };

    this.unsub = function () {
      return _this.__unsub__REACT_HOT_LOADER__.apply(_this, arguments);
    };
  }

  // ------------------------------------
  // Utils
  // ------------------------------------

  (0, _createClass3.default)(EventStack, [{
    key: '___emit__REACT_HOT_LOADER__',
    value: function ___emit__REACT_HOT_LOADER__(name) {
      var _this2 = this;

      return function (event) {
        (0, _forEach3.default)(_this2._pools, function (pool, poolName) {
          var handlers = pool[name];


          if (!handlers) return;
          if (poolName === 'default') {
            (0, _forEach3.default)(handlers, function (handler) {
              return handler(event);
            });
            return;
          }
          (0, _last3.default)(handlers)(event);
        });
      };
    }
  }, {
    key: '___normalize__REACT_HOT_LOADER__',


    // ------------------------------------
    // Listeners handling
    // ------------------------------------

    value: function ___normalize__REACT_HOT_LOADER__(handlers) {
      return (0, _isArray3.default)(handlers) ? handlers : [handlers];
    }
  }, {
    key: '___listen__REACT_HOT_LOADER__',
    value: function ___listen__REACT_HOT_LOADER__(name) {
      if ((0, _has3.default)(this._handlers, name)) return;
      var handler = this._emit(name);

      document.addEventListener(name, handler);
      this._handlers[name] = handler;
    }
  }, {
    key: '___unlisten__REACT_HOT_LOADER__',


    // ------------------------------------
    // Pub/sub
    // ------------------------------------

    value: function ___unlisten__REACT_HOT_LOADER__(name) {
      if ((0, _some3.default)(this._pools, name)) return;
      var handler = this._handlers[name];


      document.removeEventListener(name, handler);
      delete this._handlers[name];
    }
  }, {
    key: '__sub__REACT_HOT_LOADER__',
    value: function __sub__REACT_HOT_LOADER__(name, handlers) {
      var pool = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'default';

      if (!_isBrowser2.default) return;

      var events = (0, _uniq3.default)([].concat((0, _toConsumableArray3.default)((0, _get3.default)(this._pools, pool + '.' + name, [])), (0, _toConsumableArray3.default)(this._normalize(handlers))));

      this._listen(name);
      (0, _set3.default)(this._pools, pool + '.' + name, events);
    }
  }, {
    key: '__unsub__REACT_HOT_LOADER__',
    value: function __unsub__REACT_HOT_LOADER__(name, handlers) {
      var pool = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'default';

      if (!_isBrowser2.default) return;

      var events = _without3.default.apply(undefined, [(0, _get3.default)(this._pools, pool + '.' + name, [])].concat((0, _toConsumableArray3.default)(this._normalize(handlers))));

      if (events.length > 0) {
        (0, _set3.default)(this._pools, pool + '.' + name, events);
        return;
      }

      (0, _set3.default)(this._pools, pool + '.' + name, undefined);
      this._unlisten(name);
    }
  }]);
  return EventStack;
}();

var instance = new EventStack();

var _default = instance;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(EventStack, 'EventStack', 'src/lib/eventStack.js');

  __REACT_HOT_LOADER__.register(instance, 'instance', 'src/lib/eventStack.js');

  __REACT_HOT_LOADER__.register(_default, 'default', 'src/lib/eventStack.js');
}();

;